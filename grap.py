import requests
from bs4 import BeautifulSoup
import itertools
import re
import random
import time

commonWords = ['aan', 'aanbod', 'aanraken', 'aanval', 'aap', 'aardappel', 'aarde', 'aardig', 'acht', 'achter', 'actief', 'activiteit', 'ademen', 'af', 'afgelopen', 'afhangen', 'afmaken', 'afname', 'afspraak', 'afval', 'al', 'algemeen', 'alleen', 'alles', 'als', 'alsjeblieft', 'altijd', 'ander', 'andere', 'anders', 'angst', 'antwoord', 'antwoorden', 'appel', 'arm', 'auto', 'avond', 'avondeten', 'baan', 'baby', 'bad', 'bal', 'bang', 'bank', 'basis', 'bed', 'bedekken', 'bedreiging', 'bedreven', 'been', 'beer', 'beest', 'beetje', 'begin', 'begrijpen', 'begrip', 'behalve', 'beide', 'beker', 'bel', 'belangrijk', 'bellen', 'belofte', 'beneden', 'benzine', 'berg', 'beroemd', 'beroep', 'bescherm', 'beslissen', 'best', 'betalen', 'beter', 'bevatten', 'bewegen', 'bewolkt', 'bezoek', 'bibliotheek', 'bieden', 'bij', 'bijna', 'bijten', 'bijvoorbeeld', 'bijzonder', 'binnen', 'binnenkort', 'blad', 'blauw', 'blazen', 'blij', 'blijven', 'bloed', 'bloem', 'bodem', 'boek', 'boerderij', 'boete', 'boom', 'boon', 'boord', 'boos', 'bord', 'borstelen', 'bos', 'bot', 'bouwen', 'boven', 'branden', 'brandstof', 'breed', 'breken', 'brengen', 'brief', 'broer', 'broek', 'brood', 'brug', 'bruikbaar', 'bruiloft', 'bruin', 'bui', 'buiten', 'bureau', 'buren', 'bus', 'buurman', 'buurvrouw', 'cadeau', 'chocolade', 'cirkel', 'comfortabel', 'compleet', 'computer', 'conditie', 'controle', 'cool', 'correct', 'daar', 'daarom', 'dag', 'dak', 'dan', 'dansen', 'dapper', 'dat', 'de', 'deel', 'deken', 'deksel', 'delen', 'derde', 'deze', 'dichtbij', 'dienen', 'diep', 'dier', 'dik', 'ding', 'dit', 'dochter', 'doen', 'dom', 'donker', 'dood', 'door', 'doorzichtig', 'doos', 'dorp', 'draad', 'draaien', 'dragen', 'drie', 'drijven', 'drinken', 'drogen', 'dromen', 'droog', 'druk', 'dubbel', 'dun', 'dus', 'duur', 'duwen', 'echt', 'een', 'één', 'eend', 'eenheid', 'eenzaam', 'eerste', 'eeuw', 'effect', 'ei', 'eigen', 'eiland', 'einde', 'eis', 'elektrisch', 'elk', 'en', 'enkele', 'enthousiast', 'erg', 'eten', 'even', 'examen', 'extreem', 'falen ', 'familie', 'feest', 'feit', 'fel', 'fijn', 'film', 'fit', 'fles', 'foto', 'fout', 'fris', 'fruit', 'gaan', 'gat', 'gebeuren', 'gebeurtenis', 'gebied', 'geboorte', 'geboren', 'gebruik', 'gebruikelijk', 'gebruiken', 'gedrag', 'gedragen', 'geel', 'geen', 'gehoorzamen', 'geit', 'geld', 'geliefde', 'gelijk', 'geloof', 'geluid', 'geluk', 'gemak', 'gemakkelijk', 'gemeen', 'genieten', 'genoeg', 'genot', 'gerecht', 'gereedschap', 'geschikt', 'gespannen', 'geur', 'gevaar', 'gevaarlijk', 'gevangenis', 'geven', 'gevolg', 'gewicht', 'gewoon', 'gezicht', 'gezond', 'gif', 'gisteren', 'glad', 'glas', 'glimlach', 'god', 'goed', 'goedkoop', 'goud', 'graf', 'grap', 'grappig', 'gras', 'grens', 'grijs', 'groeien', 'groen', 'groente', 'groep', 'grof', 'grond', 'groot', 'grootmoeder', 'grootvader', 'haan', 'haar', 'haast', 'hal', 'halen', 'half', 'hallo', 'hamer', 'hand', 'hard', 'hart', 'haten', 'hebben', 'heel', 'heet', 'helder', 'helft', 'help', 'hem', 'hemel', 'hen', 'herfst', 'herinneren', 'hert', 'het', 'heuvel', 'hier', 'hij', 'hobby', 'hoe', 'hoed', 'hoek', 'hoeveel', 'hoeveelheid', 'hoewel', 'hond', 'honderd', 'honger', 'hoofd', 'hoog', 'hoogte', 'hoop', 'horen', 'hotel', 'houden', 'huilen', 'huis', 'hun', 'huren', 'hut', 'huur', 'idee', 'ieder', 'iedereen', 'iemand', 'iets', 'ijs', 'ijzer', 'ik', 'in', 'instrument', 'ja', 'jaar', 'jagen', 'jas', 'jij', 'jong', 'jongen', 'jouw', 'jullie', 'kaars', 'kaart', 'kaas', 'kamer', 'kans', 'kant', 'kantoor', 'kap', 'kast', 'kasteel', 'kat', 'kennen', 'kennis', 'keuken', 'keus', 'kiezen', 'kijken', 'kind', 'kip', 'kist', 'klaar', 'klas', 'klasse', 'kleden', 'klein', 'kleren', 'kleur', 'klimmen', 'klok', 'kloppen', 'klopt', 'knie', 'knippen', 'koers', 'koffer', 'koffie', 'kok', 'koken', 'kom', 'komen', 'koning', 'koningin', 'koorts', 'kop', 'kopen', 'kort', 'kost', 'kosten', 'koud', 'kraam', 'kracht', 'krant', 'krijgen', 'kruis', 'kuil', 'kunnen', 'kunst', 'laag', 'laat', 'laatst', 'lach', 'lachen', 'ladder', 'laken', 'lamp', 'land', 'lang', 'langs', 'langzaam', 'laten', 'leeftijd', 'leeg', 'leerling', 'leeuw', 'leger', 'leiden', 'lenen', 'lengte', 'lepel', 'leren', 'les', 'leuk', 'leven', 'lezen', 'lichaam', 'licht', 'liefde', 'liegen', 'liggen', 'lijk', 'lijken', 'liniaal', 'links', 'lip', 'list', 'lomp', 'lood', 'lopen', 'los', 'lot', 'lucht', 'lui', 'luisteren', 'lunch', 'maag', 'maal', 'maaltijd', 'maan', 'maand', 'maar', 'maat', 'machine', 'maken', 'makkelijk', 'mama', 'man', 'mand', 'manier', 'map', 'markeren', 'markt', 'me', 'medicijn', 'meel', 'meer', 'meerdere', 'meest', 'meisje', 'melk', 'meneer', 'mengsel', 'mensen', 'mes', 'met', 'meubel', 'mevrouw', 'middel', 'midden', 'mij', 'mijn ', 'miljoen', 'min', 'minder', 'minuut', 'mis', 'missen', 'mits', 'model', 'modern', 'moeder', 'moeilijk', 'moeten', 'mogelijk', 'mogen', 'moment', 'mond', 'mooi', 'moord', 'moorden', 'morgen', 'munt', 'muziek', 'na', 'naald', 'naam', 'naar', 'naast', 'nacht', 'nat', 'natuur', 'natuurlijk', 'nee', 'neer', 'negen', 'nek', 'nemen', 'net', 'netjes', 'neus', 'niet', 'niets', 'nieuw', 'nieuws', 'nobel', 'noch', 'nodig', 'noemen', 'nog', 'nood', 'nooit', 'noord', 'noot', 'normaal', 'nu', 'nul', 'nummer', 'object', 'oceaan', 'ochtend', 'oefening', 'of', 'offer', 'olie', 'olifant', 'om', 'oma', 'onder', 'onderwerp', 'onderzoek', 'oneven', 'ongeluk', 'ons', 'ontsnappen', 'ontbijt', 'ontdekken', 'ontmoeten', 'ontvangen', 'ontwikkelen', 'onze', 'oog', 'ooit', 'ook', 'oom', 'oor', 'oorlog', 'oorzaak', 'oost', 'op', 'opa', 'opeens', 'open', 'openlijk', 'opleiding', 'opnemen', 'oranje', 'orde', 'oud', 'ouder', 'over', 'overal', 'overeenkomen', 'overleden', 'overvallen', 'paar', 'paard', 'pad', 'pagina', 'pan', 'papa', 'papier', 'park', 'partner', 'pas', 'passeren', 'pen', 'peper', 'per', 'perfect', 'periode', 'persoon', 'piano', 'pijn', 'pistool', 'plaat', 'plaatje', 'plaats', 'plafond', 'plank', 'plant', 'plastic', 'plat', 'plattegrond', 'plein', 'plus', 'poes', 'politie', 'poort', 'populair', 'positie', 'postzegel', 'potlood', 'praten', 'presenteren', 'prijs', 'prins', 'prinses', 'privé', 'proberen', 'probleem', 'product', 'provincie', 'publiek', 'punt', 'raak', 'raam', 'radio', 'raken', 'rapport', 'recht', 'rechtdoor', 'rechts', 'rechtvaardig', 'redden', 'reeds', 'regen', 'reiken', 'reizen', 'rekenmachine', 'rennen', 'repareren', 'rest', 'restaurant', 'resultaat', 'richting', 'rijk', 'rijst', 'rijzen', 'ring', 'rok', 'rond', 'rood', 'rook', 'rots', 'roze', 'rubber', 'ruiken', 'ruimte', 'samen', 'sap', 'schaap', 'schaar', 'schaduw', 'scheiden', 'scherp', 'schetsen', 'schieten', 'schijnen', 'schip', 'school', 'schoon', 'schouder', 'schreeuw', 'schreeuwen', 'schrijven', 'schudden', 'seconde', 'sex', 'signaal', 'simpel', 'sinds', 'slaapkamer', 'slapen', 'slecht', 'sleutel', 'slim', 'slot', 'sluiten', 'smaak', 'smal', 'sneeuw', 'snel', 'snelheid', 'snijden', 'soep', 'sok', 'soms', 'soort', 'sorry', 'speciaal', 'spel', 'spelen', 'sport', 'spreken', 'springen', 'staal', 'stad', 'stap', 'start', 'station', 'steen', 'stelen', 'stem', 'stempel', 'ster', 'sterk', 'steun', 'stil', 'stilte', 'stoel', 'stof', 'stoffig', 'stom', 'stop', 'storm', 'straat', 'straffen', 'structuur', 'student', 'studie', 'stuk', 'succes', 'suiker', 'taal', 'taart', 'tafel', 'tak', 'tamelijk', 'tand', 'tante', 'tas', 'taxi', 'te', 'team', 'teen', 'tegen', 'teken', 'tekenen', 'telefoon', 'televisie', 'tellen', 'tennis', 'terug', 'terugkomst', 'terwijl', 'test', 'tevreden', 'thee', 'thuis', 'tien', 'tijd', 'titel', 'toekomst', 'toen', 'toename', 'totaal', 'traan', 'tram', 'trein', 'trekken', 'trouwen', 'trui', 'tuin', 'tussen', 'tweede', 'u', 'uit', 'uitleggen', 'uitnodigen', 'uitvinden', 'uitzoeken', 'uur', 'vaak', 'vaarwel', 'vader', 'vak', 'vakantie', 'vallen', 'vals', 'van', 'vandaag', 'vangen', 'vanmorgen', 'vannacht', 'varken', 'vast', 'vechten', 'veel', 'veer', 'veilig', 'ver', 'veranderen', 'verandering', 'verder', 'verdienen', 'verdrietig', 'verenigen', 'verf', 'vergelijkbaar', 'vergelijken', 'vergelijking', 'vergeten', 'vergeven', 'vergissen', 'verhaal', 'verhoging', 'verjaardag', 'verkeerd', 'verkopen', 'verlaten', 'verleden', 'verliezen', 'vernietigen', 'veroveren', 'verrassen', 'vers', 'verschil', 'verschrikkelijk', 'verspreiden', 'verstand', 'verstoppen', 'versturen', 'vertellen', 'vertrekken', 'vertrouwen', 'verwachten', 'verwijderen', 'verzamelen', 'verzameling', 'vet', 'vier', 'vierkant', 'vies', 'vijand', 'vijf', 'vijver', 'vinden', 'vinger', 'vis', 'vlag', 'vlees', 'vlieg', 'vliegtuig', 'vloer', 'voeden', 'voedsel', 'voelen', 'voet', 'voetbal', 'vogel', 'vol', 'volgende', 'volgorde', 'voor', 'voorbeeld', 'voorkomen', 'voorzichtig', 'voorzien', 'vork', 'vorm', 'vos', 'vouwen', 'vraag', 'vragen', 'vrede', 'vreemd', 'vreemde', 'vriend', 'vriendelijk', 'vriezen', 'vrij', 'vrijheid', 'vroeg', 'vroeger', 'vrouw', 'vullen', 'vuur', 'waar', 'waarom', 'waarschijnlijk', 'wachten', 'wakker', 'wanneer', 'want', 'wapen', 'warm', 'wassen', 'wat', 'water', 'we', 'week', 'weer', 'weg', 'welke', 'welkom', 'wens', 'wereld', 'werelddeel', 'werk', 'west', 'wetenschap', 'wie', 'wiel', 'wij', 'wijn', 'wijs', 'wild', 'willen', 'wind', 'winkel', 'winnen', 'winter', 'wissen', 'wit', 'wolf', 'wolk', 'wonder', 'woord', 'woud', 'wreed', 'zaak', 'zacht', 'zak', 'zand', 'zee', 'zeep', 'zeer', 'zeggen', 'zeil', 'zeker', 'zelfde', 'zes', 'zetten', 'zeven', 'ziek', 'ziekenhuis', 'ziel', 'zien', 'zij', 'zijn', 'zilver', 'zingen', 'zinken', 'zitten', 'zo', 'zoals', 'zoeken', 'zoet', 'zomer', 'zon', 'zonder', 'zonnig', 'zoon', 'zorg', 'zorgen', 'zou', 'zout', 'zuid', 'zulke', 'zullen', 'zus', 'zwaar', 'zwak', 'zwembad', 'zwemmen'];

class Word:
    internetRequests = 0
    def __init__(self, writtenForm):
        self.writtenForm = writtenForm
        self.pos = None
    def __repr__(self):
        return self.getDelimter() + self.writtenForm
    def getDescription(self):
        try:
            self.description
        except AttributeError:
            self.__getSynoniemenNetData()
        return self.description
    def getSynonyms(self):
        try:
            self.synonyms
        except AttributeError:
            self.__getSynoniemenNetData()
        return self.synonyms
    def getDelimter(self):
        if self.pos is None or self.pos[0] == 'b' or self.pos[0] == 't':
            return ''
        else:
            return 'een '
    def __getSynoniemenNetData(self):
        Word.internetRequests += 1
        page = requests.get('https://synoniemen.net/index.php?zoekterm=' + self.writtenForm)
        soup = BeautifulSoup(page.content, 'html.parser')
        inwbtabel_dl = soup.find('dl', class_="inwbtabel")
        if inwbtabel_dl is not None:
            self.writtenForm = inwbtabel_dl.find('dt').get_text()[:-1]
        description_dd = soup.find('dd', style="width:auto")
        self.description = None
        self.pos = None
        try:
            description_dd
        except AttributeError:
            self.description = None
        else:
            if description_dd is not None:
                matches = re.search("([^\(\)\n]+\.) (\(.+\), )?([^\(\),.;]*).*", description_dd.get_text())
                if matches is not None:
                    self.pos = matches.group(1);
                    self.description = matches.group(3).strip();
        alstrefwoordtabel = soup.find('dl', class_='alstrefwoordtabel');
        try:
            alstrefwoordtabel.find_all
        except AttributeError:
            self.synonyms = []
        else:
            meanings = alstrefwoordtabel.find_all('dd')
            self.synonyms = [[Word(x.string) for x in meaning.find_all('a')] for meaning in meanings]
            random.shuffle(self.synonyms)

jokes = 0
maxJokes = 1;

def makeJoke(word):
    print('okee, een grap over ',word,'...',sep='');
    global jokes
    global maxJokes
    if len(word.getSynonyms()) > 1:
        print(word,'heeft meerdere betekenissen, dat wordt lachen!')
        permutaties = [x for x in itertools.permutations(word.getSynonyms(),2)]
        random.shuffle(permutaties)
        # permutaties = [[word.getSynonyms()[0],word.getSynonyms()[-1]]]
        for mix in permutaties:
            print('kijken of ik iets joligs kan verzinnen voor het verschil',mix)
            differencesA = [x for x in set(mix[0]).difference(set(mix[1]))]
            differencesB = [x for x in set(mix[1]).difference(set(mix[0]))]
            random.shuffle(differencesA)
            random.shuffle(differencesB)
            if len(differencesA) == 0:
                print(mix[0], 'lijkt teveel op', mix[1])
            if len(differencesB) == 0:
                print(mix[1], 'lijkt teveel op', mix[0])
            for differenceA in differencesA:
                differenceA.getDescription()
                print('wat is het verschil tussen',differenceA,'en',word,'?')
                for differenceB in differencesB:
                    differenceB.getDescription()
                    print(differenceA,'is niet',differenceA.getDelimter(),differenceB.writtenForm)
                return
    else:
        print('ik weet niet meerdere betekenissen voor', word,'...')
        time.sleep(1)
        word = randomWord()
        makeJoke(word)

    # for synonym in word.getSynonyms():
    #     # if len(combo[0].getSynonyms()) > 0:
    #     differences = set.difference(set(synonym.getSynonyms()), set(word.getSynonyms()))
    #     for difference in differences:
    #         if difference.writtenForm != synonym.writtenForm and difference.writtenForm != word.writtenForm:
    #             difference.getDescription()
    #             print('wat is het verschil tussen ', word, ' en ', synonym, '?', sep='')
    #             print(word, ' is niet ', word.getDelimter(), difference.writtenForm,'.', sep='')
    #             jokes += 1
    #             if jokes >= maxJokes:
    #                 return
    # description = word.getDescription()
    # if description is not None:
    #     assosiation = Word(description.split(' ')[-1]);
    #     if assosiation.writtenForm != word.writtenForm:
    #         print('... of over ', assosiation,'...',sep='')
    #         makeJoke(assosiation)
    # else:
    #     print('ik weet geen mop over ',word,'!',sep='')
    #     time.sleep(1)
    #     word = randomWord()
    #     makeJoke(word)

def describe(word):
    print(word.getSynonyms())
    print(word.getDescription())
    print(word)

def randomWord():
    # file = open("OpenTaal-210G-basis-gekeurd.txt","r")
    # lines = [line.strip() for line in file]
    return Word(random.choice(commonWords))

word = randomWord()
# describe(word)
makeJoke(word)


print('(',Word.internetRequests,' requests)', sep='')
